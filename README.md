syntax:
$.fn.focusSequencer(["selector1", "selector2", "selector n"]);
$.fn.focusSequencer(["selector1", "selector2", "selector n"], 'keydown', 'selector2');

e.g.:
$.fn.focusSequencer(["#content > header > div.app-header--primary > div > div > ol > li:nth-child(1) > a", "#commit-header > article > div.commit-header.aui-group.aui-group-split > div.commit-actions.aui-item > a:nth-child(1)"]);